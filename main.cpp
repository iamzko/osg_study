#include <iostream>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/StateSet>
#include <osg/Program>
#include <osgDB/WriteFile>
#include <osgDB/ReadFile>
#include <osgViewer/ViewerEventHandlers>

void testOsg1()
{
// 创建一个用于保存几何信息的对象
    osg::ref_ptr<osg::Geometry> geom = new osg::Geometry;
// 创建四个顶点的数组
    osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array;
    geom->setVertexArray(v.get());
    v->push_back(osg::Vec3(-1.f, 0.f, -1.f));
    v->push_back(osg::Vec3(1.f, 0.f, -1.f));
    v->push_back(osg::Vec3(1.f, 0.f, 1.f));
    v->push_back(osg::Vec3(-1.f, 0.f, 1.f));
// 创建四种颜色的数组
    osg::ref_ptr<osg::Vec4Array> c = new osg::Vec4Array;
    geom->setColorArray(c.get());
    geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);
    c->push_back(osg::Vec4(1.f, 0.f, 0.f, 1.f));
    c->push_back(osg::Vec4(0.f, 1.f, 0.f, 1.f));
    c->push_back(osg::Vec4(0.f, 0.f, 1.f, 1.f));
    c->push_back(osg::Vec4(1.f, 1.f, 1.f, 1.f));
// 为唯一的法线创建一个数组
    osg::ref_ptr<osg::Vec3Array> n = new osg::Vec3Array;
    geom->setNormalArray(n.get());
    geom->setNormalBinding(osg::Geometry::BIND_OVERALL);
    n->push_back(osg::Vec3(0.f, -1.f, 0.f));
// 由保存的数据绘制四个顶点的多边形
    geom->addPrimitiveSet(
            new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));
// 向 Geode 类添加几何体（ Drawable）并返回 Geode
    osg::ref_ptr<osg::Geode> geode = new osg::Geode;
    geode->addDrawable(geom.get());
    auto ret = osgDB::writeNodeFile(*(geode), "simple.osg");
    if (!ret)
    {
        osg::notify(osg::FATAL) << "Failed in osgDB::writeNode()." << std::endl;
    }
}

void testOsgAndShader()
{
    auto group{osg::ref_ptr<osg::Group>{new osg::Group{}}};
    auto model{osg::ref_ptr<osg::Node>{osgDB::readNodeFile("simple.osg")}};
    group->addChild(model);
    auto state{osg::ref_ptr<osg::StateSet>(group->getOrCreateStateSet())};
    //属性类，添加属性
    auto texProgram{osg::ref_ptr<osg::Program>{new osg::Program{}}};
    //新建片元着色器
    auto texFragObj{osg::ref_ptr<osg::Shader>{new osg::Shader{osg::Shader::FRAGMENT}}};
    texFragObj->loadShaderSourceFromFile("/home/kai/code/osg_study/hpcv-water-29-oct-2010/shaders/hpcv-water-tile"
                                           ".frag");
    texProgram->addShader(texFragObj);
    state->setAttributeAndModes(texProgram);

    osgViewer::Viewer viewer;
    viewer.setSceneData(group);
    viewer.realize();
    auto wndPtr{dynamic_cast<osgViewer::GraphicsWindow *>(viewer.getCamera()->getGraphicsContext())};
    if (wndPtr)
    {
        wndPtr->setWindowRectangle(100, 100, 600, 600);
        wndPtr->setWindowDecoration(true);
    }
    viewer.run();
}

void testOsg3()
{
    auto cow = osg::ref_ptr<osg::Node>{osgDB::readNodeFile("/home/kai/code/osgQt/data/cow.osgt")};
    if (cow.valid())
    {
        osgDB::writeNodeFile(*cow, "/home/kai/code/cow.obj");
    }
    osg::notify(osg::WARN) << "WRITE COW TO OBJ!";
}

void testOsgLas(std::string &&filePath)
{
    auto node = osg::ref_ptr<osg::Node>{osgDB::readNodeFile(filePath)};
    if (node.valid())
    {
        osg::notify(osg::WARN) << "OPEN LAS FILE SUC!!!\n";

        osgViewer::Viewer viewer;
        viewer.setSceneData(node);
        viewer.addEventHandler(new osgViewer::StatsHandler);
        viewer.realize();
        auto wnd_ptr{dynamic_cast<osgViewer::GraphicsWindow *>(viewer.getCamera()->getGraphicsContext())};
        if (wnd_ptr)
        {
            wnd_ptr->setWindowRectangle(100, 100, 600, 600);
            wnd_ptr->setWindowDecoration(true);
        }
        viewer.run();
    }
}

int main()
{
//    testOsg3();
//    testOsgAndShader();
    testOsgLas("/mnt/C6C43178C4316BB5/清水河点云数据/清水河.las");
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
